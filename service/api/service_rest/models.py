from django.db import models
from model_utils.fields import StatusField
from model_utils import Choices


class Technician(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_id = models.CharField(max_length=150, unique=True)

    def __str__ (self):
        return f"{self.first_name} {self.last_name}"


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):

    date_time = models.DateTimeField()
    reason = models.CharField(max_length=50)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=150)
    technician = models.ForeignKey(
        Technician,
        related_name = 'appointments',
        on_delete = models.CASCADE
    )
    status = models.CharField(max_length=10, null=True)
