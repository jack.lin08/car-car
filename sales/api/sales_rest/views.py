from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .encoders import AutomobileVOEncoder, CustomerEncoder, SalespersonEncoder, SaleEncoder
from .models import Salesperson, Customer, AutomobileVO, Sale
import json


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all
        return JsonResponse(
            {"customer": salespersons},
            encoder= SalespersonEncoder
            )
    else: #POST
        content = json.loads(request.body)
        try:
            emp_id = content["employee_id"]
            employee_id = Salesperson.objects.get(id=emp_id)
            content["employee_id"] = emp_id
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee ID"},
                status=400,
            )
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all
        return JsonResponse(
            {"customer": customers},
            encoder= CustomerEncoder
            )
    else: #POST
        content = json.loads(request.body)
        try:
            phone_num = content["phone_number"]
            phone_number = Customer.objects.get(id=phone_num)
            content["phone_number"] = phone_num
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid phone number"},
                status=400,
            )
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all
        return JsonResponse(
            {"sale": sales},
            encoder= SaleEncoder
            )
    else: #POST
        content = json.loads(request.body)
        try:
            vin_num = content["vin"]
            vin = Sale.objects.get(id=vin_num)
            content["vin"] = vin_num
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid vin number"},
                status=400,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
